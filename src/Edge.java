import java.awt.*;

/**
 * Created by Arsen on 20.10.2015.
 */
public class Edge implements Comparable<Edge> {
    private Vertex from;
    private Vertex to;
    private boolean isWeighted;
    private boolean isOriented;
    private int x1, x2, y1, y2;
    private int arx1, arx2, ary1, ary2;
    private int cost = 0;
//    private Point f,t;

    public Edge(Vertex from, Vertex to, boolean isWeighted, boolean isOriented) {
        this.from = from;
        this.to = to;
        this.isWeighted = isWeighted;
        this.isOriented = isOriented;
        refresh();
    }

    public Edge(Edge e) {
        this.from = new Vertex(e.from);
        this.to = new Vertex(e.to);
        this.isWeighted = e.isWeighted;
        this.isOriented = e.isOriented;
        refresh();
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public void refresh() {
        /*x1=from.getX();
        x2=to.getX();
        y1=from.getY();
        y2=to.getY();*/
        double angleTo = 0, angleFrom = 0;
        if (to.getX() - from.getX() == 0) {
            x1 = from.getX();
            x2 = to.getX();
            if (to.getY() < from.getY()) {
                angleTo = Math.PI / 2;
                angleFrom = 3 * Math.PI / 2;
                y1 = from.getY() - Vertex.R;
                y2 = to.getY() + Vertex.R;
            } else {
                angleTo = 3 * Math.PI / 2;
                angleFrom = Math.PI / 2;
                y1 = from.getY() + Vertex.R;
                y2 = to.getY() - Vertex.R;
            }
        } else if (to.getY() - from.getY() == 0) {
            y1 = from.getY();
            y2 = to.getY();
            if (to.getX() < from.getX()) {
                angleFrom = Math.PI;
                x1 = from.getX() - Vertex.R;
                x2 = to.getX() + Vertex.R;
            } else {
                angleTo = Math.PI;
                x1 = from.getX() + Vertex.R;
                x2 = to.getX() - Vertex.R;
            }
        } else if (from.getX() > to.getX() && from.getY() > to.getY()) {
            angleTo = Math.atan((from.getY() - to.getY()) / (from.getX() - to.getX()));
            angleFrom = 3 * Math.PI / 2 - Math.atan((from.getX() - to.getX()) / (from.getY() - to.getY()));
        } else if (from.getX() > to.getX() && from.getY() < to.getY()) {
            angleTo = Math.PI * 2 - Math.atan((to.getY() - from.getY()) / (from.getX() - to.getX()));
            angleFrom = Math.PI / 2 + Math.atan((from.getX() - to.getX()) / (to.getY() - from.getY()));
        } else if (from.getX() < to.getX() && from.getY() < to.getY()) {
            angleTo = 3 * Math.PI / 2 - Math.atan((to.getX() - from.getX()) / (to.getY() - from.getY()));
            angleFrom = Math.atan((to.getY() - from.getY()) / (to.getX() - from.getX()));
        } else {
            angleFrom = Math.PI * 2 - Math.atan((from.getY() - to.getY()) / (to.getX() - from.getX()));
            angleTo = Math.PI / 2 + Math.atan((to.getX() - from.getX()) / (from.getY() - to.getY()));
        }
        x1 = from.getX() + (int) Math.round(Vertex.R * Math.cos(angleFrom));
        x2 = to.getX() + (int) Math.round(Vertex.R * Math.cos(angleTo));
        y1 = from.getY() + (int) Math.round(Vertex.R * Math.sin(angleFrom));
        y2 = to.getY() + (int) Math.round(Vertex.R * Math.sin(angleTo));
        if (isOriented) {
            double angleAr1 = 0, angleAr2 = 0;
            if (angleTo < Math.PI / 4) {
                angleAr1 = 2 * Math.PI + angleTo - Math.PI / 4;
                angleAr2 = angleTo + Math.PI / 4;
            } else if (angleTo > 7 * Math.PI / 4) {
                angleAr2 = angleTo + Math.PI / 4 - Math.PI * 2;
                angleAr1 = angleTo - Math.PI / 4;
            } else {
                angleAr1 = angleTo - Math.PI / 4;
                angleAr2 = angleTo + Math.PI / 4;
            }
            arx1 = x2 + (int) Math.round(10 * Math.cos(angleAr1));
            arx2 = x2 + (int) Math.round(10 * Math.cos(angleAr2));
            ary1 = y2 + (int) Math.round(10 * Math.sin(angleAr1));
            ary2 = y2 + (int) Math.round(10 * Math.sin(angleAr2));
        }

    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public Vertex getFrom() {
        return from;
    }

    public void setFrom(Vertex from) {
        this.from = from;
    }

    public Vertex getTo() {
        return to;
    }

    public void setTo(Vertex to) {
        this.to = to;
    }

    public boolean isWeighted() {
        return isWeighted;
    }

    public void setIsWeighted(boolean isWeighted) {
        this.isWeighted = isWeighted;
    }

    public boolean isOriented() {
        return isOriented;
    }

    public void setIsOriented(boolean isOriented) {
        this.isOriented = isOriented;
    }

    public int getArx1() {
        return arx1;
    }

    public int getArx2() {
        return arx2;
    }

    public int getAry1() {
        return ary1;
    }

    public int getAry2() {
        return ary2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if (isWeighted != edge.isWeighted) return false;
        if (isOriented != edge.isOriented) return false;
        if (!from.equals(edge.from)) return false;
        return to.equals(edge.to);

    }

    public boolean hasVertex(Vertex v) {
        return v.equals(from) || v.equals(to);
    }

    public boolean isVertexTo(Vertex v) {
        return v.equals(to);
    }

    public boolean isVertexFrom(Vertex v) {
        return v.equals(from);
    }

    public Point getCostPoint() {
        return new Point((int) ((x1 + x2) / 2), (int) ((y1 + y2) / 2));
    }

    @Override
    public int compareTo(Edge o) {
        return cost - o.cost;
    }
}
