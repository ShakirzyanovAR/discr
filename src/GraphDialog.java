import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Arsen on 06.11.2015.
 */
public class GraphDialog extends JFrame {
    Graph g;

    public GraphDialog(String title, Graph g) {
        super(title);
        this.g = g;
        setSize(600, 600);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        GraphPanel gp = new GraphPanel(g);
        gp.removeAdapter();
//        setResizable(false);
        JButton button = new JButton("OK");
        button.setBounds(265, 550, 70, 20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(button);
        gp.setBounds(0, 0, 600, 540);
        add(gp);

        setResizable(false);
        setAlwaysOnTop(true);
        setVisible(true);
    }

}
