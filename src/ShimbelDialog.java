import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Arsen on 24.11.2015.
 */
public class ShimbelDialog extends JFrame{
    public ShimbelDialog(int [][]g, int numOfSteps) {
        setLayout(null);
        setTitle("Ядро графа");
        setSize(600, 600);
        JPanel jp=new JPanel(null);
        jp.setBounds(0,0,600,600);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        JButton button = new JButton("OK");
        button.setBounds(265, 550, 70, 20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        jp.add(button);
        JTable table=new JTable(new ShimbellTableModel(g));
        JLabel jLabel=new JLabel("Путь через " + numOfSteps + " ребра");
        jLabel.setBounds(5,5,200,30);
        table.setBounds(40, 40, 600, 540);
        jp.add(table);
        jp.add(jLabel);
        jp.setVisible(true);
        add(jp);
        setResizable(false);
        setAlwaysOnTop(true);
        setVisible(true);
    }
}
