import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Arsen on 22.11.2015.
 */
public class ClickDialog extends JFrame {
    public ClickDialog(String[] in) {
        setTitle("Клика графа");
        setSize(600, 600);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        JButton button = new JButton("OK");
        button.setBounds(265, 550, 70, 20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(button);
        JTextArea textArea;
        if (in.length > 0) {
            textArea = new JTextArea("Множества вершин графа которые являются кликами:\n");
            int max=Integer.MIN_VALUE;
            for (String s : in)
                if(s.length()>max)
                    max=s.length();
            for (String s : in) {
                if(s.length()==max)
                    textArea.append("{" + s + "}\n");
            }
        } else {
            textArea = new JTextArea("В данном графе нет клики");
        }
        textArea.setEditable(false);
        textArea.setBounds(0, 0, 600, 540);
        add(textArea);
        setResizable(false);
        setAlwaysOnTop(true);
        setVisible(true);
    }
}
