import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Arsen on 24.11.2015.
 */
public class ParallelForm {
    public static ArrayList<String> getParallelForm(Graph graph) {
        int g[][] = graph.getMatrix();
        int g2[][] = g.clone();
        for (int i = 0; i < g2.length; ++i)
            for (int j = 0; j < g2.length; ++j)
                g2[i][j] = g[i][j];
        ArrayList<ArrayList<Integer>> arr = new ArrayList<ArrayList<Integer>>();
        while (!allChecked(g2)) {
            ArrayList<Integer> stage = getCols(g2);
            for (int tmp : stage)
                g2 = delete(g2, tmp - 1);
            arr.add(stage);
        }
        ArrayList<String> res = new ArrayList<String>();
        for (ArrayList<Integer> tmp : arr) {
            String s = "{";
            for (int i = 0; i < tmp.size(); ++i) {
                s += (char) (tmp.get(i) + 'A' - 1) + ",";
            }
            s = s.substring(0, s.length() - 1) + '}';
            res.add(s);
        }
        return res;
    }

    private static boolean allChecked(int in[][]) {//���������� true ���� ��� �������� ������� == -1
        boolean flag = true;
        for (int tmp[] : in) {
            if (!flag)
                break;
            for (int tmp2 : tmp)
                if (tmp2 >= 0) {
                    flag = false;
                    break;
                }
        }
        return flag;
    }

    private static ArrayList<Integer> getCols(int[][] g) {//���������� "�������" �������
        ArrayList<Integer> res = new ArrayList<Integer>();
        for (int i = 0; i < g.length; i++) {
            if (!isDeletedCol(g, i)) {
                boolean flag = true;
                for (int j = 0; j < g.length; j++) {
                    if (g[i][j] > 0) {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    res.add(i + 1);
            }

        }
        return res;
    }

    private static boolean isDeletedCol(int g[][], int col) {//�������� ����������� ��������
        boolean flag = true;
        for (int i = 0; i < g.length; ++i) {
            if (g[i][col] >= 0) {
                flag = false;
                break;
            }

        }
        return flag;
    }

    private static int[][] delete(int[][] g, int colNum) {
        for (int i = 0; i < g.length; ++i)
            g[i][colNum] = -1;
        for (int i = 0; i < g.length; ++i)
            g[colNum][i] = -1;
        return g;
    }
}
