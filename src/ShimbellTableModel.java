import javax.swing.table.AbstractTableModel;

/**
 * Created by Arsen on 24.11.2015.
 */
public class ShimbellTableModel extends AbstractTableModel {
    int[][]g;

    public ShimbellTableModel(int[][] g) {
        this.g = g;
    }

    @Override
    public int getRowCount() {
        return g.length+1;
    }

    @Override
    public int getColumnCount() {
        return g.length+1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if(rowIndex==0&&columnIndex==0){
            return " ";
        }
        else if(rowIndex>0&&columnIndex>0){
            return Integer.toString(g[rowIndex-1][columnIndex-1]);
        }
        else if(rowIndex==0){
            return ""+(char)('A'+columnIndex-1);
        }
        else{
            return ""+(char)('A'+rowIndex-1);
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
