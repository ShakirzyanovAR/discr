import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * Created by Arsen on 06.11.2015.
 */
public class DijkstraAlogorythm {
    //    private static ArrayList<Vertex> usedVertexes=new ArrayList();
    private Graph g;

    public DijkstraAlogorythm() {
    }

    public Graph getDeijkstra(Graph g) {
        this.g = g;
        int[] marks = new int[g.getVertexes().size()];
        Graph resGraph = new Graph(g.isOriented(), g.isWheighted());
        resGraph.setVertexes(g.getVertexes());
        Vertex[] roots = new Vertex[g.getVertexes().size()];
        for (int i = 1; i < marks.length; ++i)
            marks[i] = Integer.MAX_VALUE;
        PriorityQueue<Edge> pq = new PriorityQueue<Edge>();
        for (Edge e : g.getEdgesFrom(g.getVertexes().get(0)))
            pq.add(e);
//        usedVertexes.add(g.getVertexes().get(0));
        while (!pq.isEmpty()) {
            Edge currEdge = pq.poll();
            if (g.isOriented()) {
                if (marks[currEdge.getTo().getLetter() - 'A'] > marks[currEdge.getFrom().getLetter() - 'A'] + currEdge.getCost()) {
                    marks[currEdge.getTo().getLetter() - 'A'] = marks[currEdge.getFrom().getLetter() - 'A'] + currEdge.getCost();
                    for (Edge e : g.getEdgesFrom(currEdge.getTo()))
                        pq.add(e);
                    roots[currEdge.getTo().getLetter() - 'A'] = currEdge.getFrom();
                }
            } else {
                Vertex vTo, vFrom;
                if (marks[currEdge.getFrom().getLetter() - 'A'] > marks[currEdge.getTo().getLetter() - 'A']) {
                    vTo = currEdge.getFrom();
                    vFrom = currEdge.getTo();
                } else {
                    vTo = currEdge.getTo();
                    vFrom = currEdge.getFrom();
                }
                if (marks[vTo.getLetter() - 'A'] > marks[vFrom.getLetter() - 'A'] + currEdge.getCost()) {
                    marks[vTo.getLetter() - 'A'] = marks[vFrom.getLetter() - 'A'] + currEdge.getCost();
                    for (Edge e : g.getEdgesFrom(vTo))
                        pq.add(e);
                    roots[vTo.getLetter() - 'A'] = vFrom;
                }
            }
        }
        ArrayList<Edge> edges = new ArrayList<Edge>();
        for (int i = 1; i < roots.length; ++i) {
            if (roots[i] != null) {
                Edge e = new Edge(roots[i], g.getVertexes().get(i), g.isWheighted(), g.isOriented());

                e.setCost(marks[i] - marks[roots[i].getLetter() - 'A']);
                edges.add(e);
            }
        }
        resGraph.setEdges(edges);

        return resGraph;
    }
}
