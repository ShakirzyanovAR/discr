import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Arsen on 16.11.2015.
 */
public class ClickAndCore {
    private Graph g;
    private int[][]matrix;

    public ClickAndCore(Graph g) {
        this.g = g;
    }

    public String[] internalStability() {
        int[][] tmp = g.getMatrix();
        String cnf = new String();
        for (int i = 0; i < tmp.length; ++i)
            for (int j = 0; j < tmp.length; ++j)
                if (tmp[i][j] != 0)
                    cnf += "(" + (char) ('A' + i) + "+" + (char) ('A' + j) + ")";
        String dnf = CNFtoDNF.DNFtoCNF(cnf);

        StringTokenizer strTok = new StringTokenizer(dnf, "+");
        String res[] = new String[strTok.countTokens()];
        int ind = 0;
        while (strTok.hasMoreTokens()) {
            String s = strTok.nextToken();
            String tmpV = new String();
            for (int i = 0; i < tmp.length; ++i)
                if (!s.contains(Character.toString((char) ('A' + i))))
                    tmpV += (char) ('A' + i) + ",";
                tmpV = tmpV.substring(0, tmpV.length() - 1);
            res[ind] = tmpV;
            ++ind;
        }
        return res;
    }

    public String[] internalStability(int[][] inv) {
        int[][] tmp = inv.clone();
        String cnf = new String();
        for (int i = 0; i < tmp.length; ++i)
            for (int j = i; j < tmp.length; ++j)
                if (tmp[i][j] != 0)
                    cnf += "(" + (char) ('A' + i) + "+" + (char) ('A' + j) + ")";
        String dnf = CNFtoDNF.DNFtoCNF(cnf);

        StringTokenizer strTok = new StringTokenizer(dnf, "+");
        String res[] = new String[strTok.countTokens()];
        int ind = 0;
        while (strTok.hasMoreTokens()) {
            String s = strTok.nextToken();
            String tmpV = new String();
            for (int i = 0; i < tmp.length; ++i)
                if (!s.contains(Character.toString((char) ('A' + i))))
                    tmpV += (char) ('A' + i) + ",";
            tmpV = tmpV.substring(0, tmpV.length() - 1);
            res[ind] = tmpV;
            ++ind;
        }
        return res;
    }

    public ArrayList<ArrayList<Character>> getCore() {
        matrix=g.getMatrix();
        String[] ext = externalStability();
        String[] intern = internalStability();
        ArrayList<String> core = new ArrayList<String>();
        for (int i = 0; i < ext.length; ++i)
            for (int j = 0; j < intern.length; ++j)
                if (ext[i].equals(intern[j]))
                    core.add(ext[i]);
        System.out.println(core.size());
        ArrayList<ArrayList<Character>> res = new ArrayList<ArrayList<Character>>();
        for (int i = 0; i < core.size(); ++i) {
            StringTokenizer strTok = new StringTokenizer(core.get(i), ", ");
            ArrayList<Character> chars = new ArrayList<Character>();
            while (strTok.hasMoreTokens())
                chars.add(strTok.nextToken().charAt(0));
            res.add(chars);
        }
        return res;
    }

    public String[] externalStability() {
        int[][] tmp = matrix;
        String cnf = new String();
        for(int i=0;i<matrix.length;++i){
            tmp[i][i]=1;
        }
        for (int i = 0; i < tmp.length; ++i) {
            cnf += '(';
            for (int j = 0; j < tmp.length; ++j)
                if (tmp[i][j] != 0)
                    cnf += (char) ('A' + j) + "+";
            cnf = cnf.substring(0, cnf.length() - 1) + ")";
        }
        String dnf = CNFtoDNF.DNFtoCNF(cnf);
        StringTokenizer strTok = new StringTokenizer(dnf, "+");
        String res[] = new String[strTok.countTokens()];
        int ind = 0;
        while (strTok.hasMoreTokens()) {
            String s = strTok.nextToken();
            String tmpV = new String();
            for (int i = 0; i < tmp.length; ++i)
                if (s.contains(Character.toString((char) ('A' + i))))
                    tmpV += (char) ('A' + i) + ",";
            if (tmpV.length() > 1) {
                tmpV = tmpV.substring(0, tmpV.length() - 1);
                res[ind] = tmpV;
            }
            ++ind;
        }
        return res;
    }

    public String[] click() {
        int inverted[][] = g.getMatrix().clone();
        for (int i = 0; i < inverted.length; ++i)
            for (int j = 0; j < inverted.length; ++j)
                if (i >= j || (i < j && inverted[i][j] > 0))
                    inverted[i][j] = 0;
                else inverted[i][j] = 1;
        String[] res = internalStability(inverted).clone();
        return res;

    }
}
