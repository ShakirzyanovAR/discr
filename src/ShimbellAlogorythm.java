/**
 * Created by Arsen on 24.11.2015.
 */
public class ShimbellAlogorythm {
    private static int[][] first;
    public static int[][] getShimbell(int[][]g,int numOfEdges){
        first=g.clone();
        for(int i=0;i<numOfEdges-1;++i){
            g=mult(g);
        }
        return g;
    }
    private static int[][]mult(int g[][]){
        int [][]res=new int[g.length][g.length];
        for(int i=0;i<first.length;++i){
            for(int j=0;j<g.length;++j){
                int min=Integer.MAX_VALUE;
                for(int k=0;k<g.length;++k){
                    if(first[k][i]!=0 && g[j][k]!=0 && first[k][i]+g[j][k]<min )
                        min=first[k][i]+g[j][k];
                }
                if(min!=Integer.MAX_VALUE)
                    res[j][i]=min;
                else res[j][i]=0;
            }
        }
        return res;
    }
}
