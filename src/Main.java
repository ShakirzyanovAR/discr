import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by Arsen on 23.10.2015.
 */
public class Main {
    static GraphPanel gp;

    public static void main(String[] args) {
        final JFrame jFrame = new JFrame("Graph");
        gp = new GraphPanel(true, true);
//        gp.setLayout(null);
        jFrame.setSize(800, 600);
        gp.setBounds(0, 0, 600, 565);
        JPanel mainPanel = new JPanel(null);

        JLabel jLabel1 = new JLabel("Ориентированность графа:");
        jLabel1.setBounds(610, 10, 160, 20);
        JComboBox<String> jComboBox1 = new JComboBox<String>(new String[]{"Ориентированный", "Не ориентированный"});
        jComboBox1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getItem().equals("Ориентированный")) {
                    gp.setIsOriented(true);
                }
                if (e.getItem().equals("Не ориентированный")) {
                    gp.setIsOriented(false);
                }
            }
        });
        jComboBox1.setBounds(610, 40, 160, 20);

        JLabel jLabel2 = new JLabel("Взвешенность графа:");
        jLabel2.setBounds(610, 70, 160, 20);
        JComboBox<String> jComboBox2 = new JComboBox<String>(new String[]{"Взвешенный", "Не взвешенный"});
        jComboBox2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getItem().equals("Взвешенный")) {
                    gp.setIsWheighted(true);
                }
                if (e.getItem().equals("Не взвешенный")) {
                    gp.setIsWheighted(false);
                }
            }
        });
        jComboBox2.setBounds(610, 100, 160, 20);


        JLabel jLabel3 = new JLabel("Алгоритмы:");
        jLabel3.setBounds(610, 130, 160, 20);
        final JComboBox<String> algCombBox = new JComboBox<String>(new String[]{"Алгоритм Дейкстры",
                "Алгоритм Шимбелла",
                "Ядро графа",
                "Клика графа",
                "ЯПФ", "Алгоритм Краскалла"});
        algCombBox.setBounds(610, 160, 160, 20);

        JButton jBtn = new JButton("<html><p>Результат</p><p>работы алгоритма</html>");
        jBtn.setBounds(610, 290, 160, 40);
        jBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(gp.getG().isEmpty()){
                    JOptionPane.showMessageDialog(jFrame, "Необходимо ввести ребра");
                    return ;
                }
                if (gp.getG().getVertexes().size() == 0) {
                    JOptionPane.showMessageDialog(jFrame, "Введен пустой граф","Ошибка",JOptionPane.ERROR_MESSAGE);
                } else if (algCombBox.getSelectedItem().equals("Алгоритм Дейкстры")) {
                    if (!gp.getG().isWheighted())
                        JOptionPane.showMessageDialog(jFrame, "Алгоритм Дейкстры работает только с взвешенным графом","Ошибка",JOptionPane.ERROR_MESSAGE);
                    else {
                        GraphDialog gd = new GraphDialog("Алгоритм Дейкстры", new DijkstraAlogorythm().getDeijkstra(gp.getG()));
                    }
                } else if (algCombBox.getSelectedItem().equals("Алгоритм Краскалла")) {
                    if (!gp.getG().isWheighted())
                        JOptionPane.showMessageDialog(jFrame, "Алгоритм Краскалла работает только с взвешенным графом","Ошибка",JOptionPane.ERROR_MESSAGE);
                    else if (gp.getG().isOriented())
                        JOptionPane.showMessageDialog(jFrame, "Граф должен быть неориентированным","Ошибка",JOptionPane.ERROR_MESSAGE);
                    else {
                        GraphDialog gd = new GraphDialog("Алгоритм Крускалла", new Kruskall().getKruskall(gp.getG()));
                    }
                } else if (algCombBox.getSelectedItem().equals("Ядро графа")) {
                    if (!gp.getG().isOriented())
                        JOptionPane.showMessageDialog(jFrame, "Граф должен быть ориентированным","Ошибка",JOptionPane.ERROR_MESSAGE);
                    else {
                        Graph coreGraph = new Graph(gp.getG());
                        ClickAndCore clickAndCore = new ClickAndCore(coreGraph);
                        CoreDialog cd = new CoreDialog(clickAndCore.getCore());
                    }
                } else if (algCombBox.getSelectedItem().equals("Клика графа")) {
                    /*if(!gp.getG().isOriented())
                        JOptionPane.showMessageDialog(jFrame,"Граф должен быть ориентированным");
                    else {*/
                    Graph coreGraph = new Graph(gp.getG());
                    ClickAndCore clickAndCore = new ClickAndCore(coreGraph);
                    ClickDialog cd = new ClickDialog(clickAndCore.click());
//                    }
                } else if (algCombBox.getSelectedItem().equals("ЯПФ")) {
                    if (gp.getG().isWithCycle()) {
                        JOptionPane.showMessageDialog(jFrame, "Граф должен быть ацикличным","Ошибка",JOptionPane.ERROR_MESSAGE);
                    } else {
                        new ParallelDialog(ParallelForm.getParallelForm(gp.getG()));
                    }
                }
                else if(algCombBox.getSelectedItem().equals("Алгоритм Шимбелла")){
                    if(!gp.getG().isWheighted()){
                        JOptionPane.showMessageDialog(jFrame, "Граф должен быть взвешенным","Ошибка",JOptionPane.ERROR_MESSAGE);
                    }
                    else{
                        DialogShimbell ds=new DialogShimbell(gp.getG());
                    }
                }

            }
        });

        mainPanel.add(jLabel1);
        mainPanel.add(jLabel2);
        mainPanel.add(jLabel3);
        mainPanel.add(jComboBox1);
        mainPanel.add(jComboBox2);
        mainPanel.add(algCombBox);
        mainPanel.add(jBtn);
        mainPanel.add(gp);
        jFrame.setResizable(false);
        jFrame.add(mainPanel);
//        jFrame.add(gp);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setVisible(true);
    }

}
