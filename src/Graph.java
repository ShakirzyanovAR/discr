import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Arsen on 20.10.2015.
 */
public class Graph {
    private boolean isOriented;
    private boolean isWheighted;
    private ArrayList<Edge> graph;
    private ArrayList<Vertex> vertexes;

    public Graph(boolean isOriented, boolean isWheighted) {
        graph = new ArrayList<Edge>();
        vertexes = new ArrayList<Vertex>();
        this.isOriented = isOriented;
        this.isWheighted = isWheighted;
    }

    public Graph(Graph g) {
        this.isOriented = g.isOriented;
        this.isWheighted = g.isWheighted;
        this.graph = new ArrayList<Edge>(g.graph);
        this.vertexes = new ArrayList<Vertex>(g.vertexes);
    }

    public Graph(boolean isOriented, boolean isWheighted, ArrayList<Edge> graph, ArrayList<Vertex> vertexes) {
        this.isOriented = isOriented;
        this.isWheighted = isWheighted;
        this.graph = graph;
        this.vertexes = vertexes;
    }

    public ArrayList<Edge> getEdges() {
        return graph;
    }

    public boolean isOriented() {
        return isOriented;
    }

    public void setColorByLetterArray(ArrayList<Character> letters) {
        for (Vertex v : vertexes)
            v.setColor(Color.BLACK);
        for (Vertex v : vertexes) {
            if (letters.contains(v.getLetter())) {
                v.setColor(Color.YELLOW);
            }
        }
    }

    public void setIsOriented(boolean isOriented) {
        this.isOriented = isOriented;
        for (Edge e : graph)
            e.setIsOriented(isOriented);
    }

    public void clearColor() {
        for (Vertex v : vertexes)
            v.setColor(Color.BLACK);
    }

    public boolean isWheighted() {
        return isWheighted;
    }

    public void setIsWheighted(boolean isWheighted) {
        this.isWheighted = isWheighted;
        for (Edge e : graph)
            e.setIsWeighted(isWheighted);
    }

    public ArrayList<Vertex> getVertexes() {
        return vertexes;
    }

    public void addVertex(Vertex v) {
        vertexes.add(v);
    }

    public void deleteVertex(Vertex v) {
        ArrayList<Edge> toDelete = new ArrayList<Edge>();
        for (Edge e : graph)
            if (e.getFrom() == v || e.getTo() == v)
                toDelete.add(e);
        graph.removeAll(toDelete);
        int ind = vertexes.indexOf(v);
        for (int i = ind; i < vertexes.size(); ++i) {
            vertexes.get(i).setLetter((char) (vertexes.get(i).getLetter() - 1));
        }
        vertexes.remove(v);
    }
    public boolean isEmpty(){
        return graph.size()==0;
    }
    public void addEdge(Edge e) {
        graph.add(e);
    }

    public void deleteEdge(Edge e) {
        graph.remove(e);
    }

    public void setEdges(ArrayList<Edge> graph) {
        this.graph = graph;
    }

    public void setVertexes(ArrayList<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    public int[][] getMatrix() {
        int[][] res = new int[vertexes.size()][vertexes.size()];
        for (Edge e : graph) {
            if (isOriented && isWheighted)
                res[e.getFrom().getLetter() - 'A'][e.getTo().getLetter() - 'A'] = e.getCost();
            else if (isOriented) {
                res[e.getFrom().getLetter() - 'A'][e.getTo().getLetter() - 'A'] = 1;
            } else if (isWheighted) {
                res[e.getFrom().getLetter() - 'A'][e.getTo().getLetter() - 'A'] = e.getCost();
                res[e.getTo().getLetter() - 'A'][e.getFrom().getLetter() - 'A'] = e.getCost();
            } else {
                res[e.getFrom().getLetter() - 'A'][e.getTo().getLetter() - 'A'] = 1;
                res[e.getTo().getLetter() - 'A'][e.getFrom().getLetter() - 'A'] = 1;
            }
        }
        return res;
    }

    public ArrayList<Edge> getEdgesFrom(Vertex v) {
        ArrayList<Edge> res = new ArrayList<Edge>();
        for (Edge e : graph)
            if (isOriented()) {
                if (e.isVertexFrom(v)) res.add(e);
            } else if (e.hasVertex(v)) res.add(e);
        return res;
    }

    public boolean isWithCycle() {
        isCycled = false;
        matrix = getMatrix();
        used = new boolean[matrix.length];
        for (int j = 0; j < used.length; ++j)
            used[j] = false;

        for (int i = 0; i < matrix.length; i++) {
            start=i;
            DFS(-1, i);
            if (isCycled) {
                return true;
            } else
                for (int j = 0; j < used.length; ++j)
                    used[j] = false;
            isCycled = false;
        }
        return false;
    }

    int start;
    private int[][] matrix;
    private boolean[] used;
    private boolean isCycled;

    private void DFS(int prev, int v) {
        used[v] = true;
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[v][i] > 0 && v != i && i != prev) {
                if (i==start) {
                    isCycled = true;
                    System.out.println(v+" "+i);
                }
                else
                    DFS(v, i);
            }

        }


    }
}
