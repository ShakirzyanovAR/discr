import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Created by Arsen on 24.09.2015.
 */
public class CNFtoDNF {
    static String DNFtoCNF(String in) {
        ArrayList<String> parsed = parse(in);//���� ��������� ������ �� ���������
        String res = distr(parsed);//��������� ��������������� �����
        res = remove(res);//������� ���������� ��������
        res = res.substring(0, res.length() - 1);
        return res;
    }

    private static ArrayList<String> parse(String in) {
        StringTokenizer strTok = new StringTokenizer(in, "()");
        ArrayList<String> res = new ArrayList<String>();
        while (strTok.hasMoreTokens())
            res.add(strTok.nextToken());
        return res;
    }

    private static String distr(ArrayList<String> strings) {
        String base = strings.remove(0);
        for (int i = 0; i < strings.size(); ++i) {
            StringTokenizer strTokFirst = new StringTokenizer(base, "+");
            StringTokenizer strTokSec = new StringTokenizer(strings.get(i), "+");
            ArrayList<String> firstArr = new ArrayList<String>();
            ArrayList<String> secArr = new ArrayList<String>();
            while (strTokFirst.hasMoreTokens())
                firstArr.add(strTokFirst.nextToken());
            while (strTokSec.hasMoreTokens())
                secArr.add(strTokSec.nextToken());
            base = "";
            for (int j = 0; j < firstArr.size(); ++j)
                for (int k = 0; k < secArr.size(); k++) {
                    if (firstArr.get(j).equals(secArr.get(k)))
                        base += firstArr.get(j);
                    else
                        base += firstArr.get(j) + secArr.get(k);
                    base += "+";
                }
        }
        return base;
    }

    private static String remove(String in) {
        StringTokenizer strTok = new StringTokenizer(in, "+");
        ArrayList<String> strings = new ArrayList<String>();
        while (strTok.hasMoreTokens()) {//������� ���������� ������� � ����������
            String s = strTok.nextToken();
            Set<Character> set = new HashSet<Character>();
            for (int i = 0; i < s.length(); ++i)
                set.add(s.charAt(i));
            s = "";
            for (char c : set)
                s += c;
            strings.add(s);
        }
        int num = strings.size();
        for (int k = 0; k < num; ++k)
            for (int i = 0; i < strings.size(); ++i) {
                String first = strings.get(i);
                boolean flag = true;
                for (int j = 0; j < strings.size(); ++j) {
                    String sec = strings.get(j);
                    if (i != j && isRemoved(first, sec)) {
                        flag = false;
                        break;
                    }
                }
                if (!flag) {
                    strings.remove(i);
                    break;
                }
            }
        String res = "";
        for (String s : strings)
            res += s + "+";
        return res;
    }

    private static boolean isRemoved(String first, String sec) {
        Set<Character> firstSet = new HashSet<Character>();
        Set<Character> secSet = new HashSet<Character>();
        for (int i = 0; i < first.length(); i++)
            firstSet.add(first.charAt(i));
        for (int i = 0; i < sec.length(); i++)
            secSet.add(sec.charAt(i));
        if (first.length() < sec.length()) return false;
        secSet.removeAll(firstSet);
        return secSet.size() == 0;
    }
}
