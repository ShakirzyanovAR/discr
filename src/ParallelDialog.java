import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Arsen on 24.11.2015.
 */
public class ParallelDialog extends JFrame {
    public ParallelDialog(ArrayList<String> par) {
        setTitle("Ядро графа");
        setSize(600, 600);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        JButton button = new JButton("OK");
        button.setBounds(265, 550, 70, 20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(button);
        JTextArea textArea;
        textArea = new JTextArea("Множества вершин по ярусам:\n");
        int ind = 1;
        for (int i = par.size() - 1; i >= 0; --i) {
            textArea.append("Множество вершин " + ind + "-го яруса:" + par.get(i) + '\n');
            ind++;
        }
        textArea.setEditable(false);
        textArea.setBounds(0, 0, 600, 540);
        add(textArea);
        setResizable(false);
        setAlwaysOnTop(true);
        setVisible(true);
    }
}
