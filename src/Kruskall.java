import java.util.PriorityQueue;

/**
 * Created by Arsen on 10.11.2015.
 */
public class Kruskall {
    public Kruskall() {
    }

    public Graph getKruskall(Graph g) {
        PriorityQueue<Edge> pq = new PriorityQueue<Edge>();
        for (Edge e : g.getEdges())
            pq.add(e);
        Graph resGraph = new Graph(g.isOriented(), g.isWheighted());
        resGraph.setVertexes(g.getVertexes());
        while (!pq.isEmpty()) {
            Edge currEdge = pq.poll();
            resGraph.addEdge(currEdge);
            if (resGraph.isWithCycle()) resGraph.deleteEdge(currEdge);
        }
        return resGraph;
    }
}
