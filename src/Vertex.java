import java.awt.*;

/**
 * Created by Arsen on 20.10.2015.
 */
public class Vertex implements Comparable<Vertex> {
    private int x, y;
    public static final int R = 10;
    private char letter;
    private Color color;

    public Vertex(int x, int y, char letter, Color color) {
        this.x = x;
        this.y = y;
        this.letter = letter;
        this.color = color;
    }

    public Vertex(Vertex v) {
        x = v.x;
        y = v.y;
        letter = v.letter;
        color = new Color(v.color.getRGB());
    }


    public boolean pointBeside(int xP, int yP) {
        return Math.pow(xP - x, 2) + Math.pow(yP - y, 2) <= R * R;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vertex vertex = (Vertex) o;

        if (x != vertex.x) return false;
        if (y != vertex.y) return false;
        return letter == vertex.letter;

    }

    @Override
    public String toString() {
        return "Vertex{" +
                "x=" + x +
                ", y=" + y +
                ", letter=" + letter +
                '}';
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public int compareTo(Vertex o) {
        return letter - o.letter;
    }
}
