import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Arsen on 21.11.2015.
 */
public class CoreDialog extends JFrame {
    public CoreDialog(ArrayList<ArrayList<Character>> cores) {
        setTitle("Ядро графа");
        setSize(600, 600);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        JButton button = new JButton("OK");
        button.setBounds(265, 550, 70, 20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(button);
        JTextArea textArea;
        if (cores.size() > 0) {
            textArea = new JTextArea("Множества вершин графа которые являются ядрами:\n");
            for (ArrayList<Character> chars : cores) {
                textArea.append("{");
                for (char c : chars)
                    textArea.append(c + ",");
                textArea.append("}\n");
            }
        } else {
            textArea = new JTextArea("В данном графе нет ядра");
        }
        textArea.setEditable(false);
        textArea.setBounds(0, 0, 600, 540);
        add(textArea);
        setResizable(false);
        setAlwaysOnTop(true);
        setVisible(true);
    }
}
