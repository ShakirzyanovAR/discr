import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Created by Arsen on 24.11.2015.
 */
public class DialogShimbell extends JFrame {
    private JTextField tf;
    private int [][]matrix;
    public DialogShimbell(Graph g){
        setSize(300,150);
        matrix=g.getMatrix();
        setLayout(null);
        setTitle("Алгоритм Шимбелла");
        JLabel jl=new JLabel("Введите кол-во ребер");
        jl.setBounds(85,3,170,20);
        add(jl);
        tf=new JTextField(6);
        tf.setDocument(new NumberDocument());
        tf.setBounds(115,25,50,20);
        add(tf);
        JButton btn=new JButton("OK");
        btn.setBounds(108,65,70,20);
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(tf.getText().length()!=0 && Integer.parseInt(tf.getText())>1){
                    new ShimbelDialog(ShimbellAlogorythm.getShimbell(matrix,Integer.parseInt(tf.getText())),Integer.parseInt(tf.getText()));
                    dispose();
                }
            }
        });
        add(btn);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }
}
