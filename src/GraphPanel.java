import javafx.scene.shape.Line;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by Arsen on 20.10.2015.
 */
public class GraphPanel extends JPanel {
    private Graph g;
    private Line line = null;
    private boolean isOriented;
    private boolean isWheighted;
    private Adapter adapter;

    public GraphPanel(boolean isOriented, boolean isWheighted) {
        adapter = new Adapter();
        this.isOriented = isOriented;
        this.isWheighted = isWheighted;
        addMouseListener(adapter);
        addMouseMotionListener(adapter);
        this.g = new Graph(isOriented, isWheighted);
        setBackground(new Color(212, 208, 208));
    }

    public GraphPanel(Graph g) {
        this.g = g;
//        Adapter adapter=new Adapter();
        addMouseListener(adapter);
        addMouseMotionListener(adapter);
        setBackground(new Color(212, 208, 208));
    }

    public boolean isOriented() {
        return isOriented;
    }

    public void setIsOriented(boolean isOriented) {
        this.isOriented = isOriented;
        g.setIsOriented(isOriented);
        repaint();
    }

    public boolean isWheighted() {
        return isWheighted;
    }

    public void setIsWheighted(boolean isWheighted) {
        if (!this.isWheighted) {
            ArrayList<Edge> edges = (ArrayList<Edge>) g.getEdges().clone();
            for (Edge e : edges) {
                g.deleteEdge(e);
            }
        }
        this.isWheighted = isWheighted;
        g.setIsWheighted(isWheighted);
        repaint();
    }

    public Graph getG() {
        return g;
    }

    public void setG(Graph g) {
        this.g = g;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;
        g2D.setColor(Color.BLACK);
        ArrayList<Vertex> vertexes = this.g.getVertexes();
        if (line != null)
            g2D.drawLine((int) line.getStartX(), (int) line.getStartY(), (int) line.getEndX(), (int) line.getEndY());
        ArrayList<Edge> edges = this.g.getEdges();
        for (Edge e : edges) {
            g2D.drawLine(e.getX1(), e.getY1(), e.getX2(), e.getY2());
            if (e.isOriented()) {
                g2D.drawLine(e.getX2(), e.getY2(), e.getArx1(), e.getAry1());
                g2D.drawLine(e.getX2(), e.getY2(), e.getArx2(), e.getAry2());
            }
            g2D.setColor(Color.BLUE);
            if (e.isWeighted()) {
                if (e.getCost() != 0)
                    g2D.drawString(String.valueOf(e.getCost()), (int) e.getCostPoint().getX(), (int) e.getCostPoint().getY());
            }
            g2D.setColor(Color.BLACK);
        }
        for (Vertex v : vertexes) {
            g2D.setColor(v.getColor());
            g2D.fillOval(v.getX() - Vertex.R, v.getY() - Vertex.R, Vertex.R * 2, Vertex.R * 2);
//            g2D.drawOval();
            g2D.setColor(Color.CYAN);
            g2D.drawString("" + v.getLetter(), v.getX() - Vertex.R / 3, v.getY() + Vertex.R / 3);
            g2D.setColor(Color.BLACK);
        }


    }
    private Edge addableEdge;
    private JFormattedTextField num;
    class Adapter extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e);
            if (SwingUtilities.isLeftMouseButton(e) && e.isShiftDown() && line==null) {
                Vertex vertex = null;
                for (Vertex vertexes : g.getVertexes())
                    if (vertexes.pointBeside(e.getX(), e.getY())) {
                        vertex = vertexes;
                        break;
                    }
                selectedVertexFrom = null;
                selectedVertexTo = null;
                g.deleteVertex(vertex);
                repaint();
            } else if (SwingUtilities.isLeftMouseButton(e) && line==null) {
                int x, y;
                if (e.getX() - Vertex.R < 0)
                    x = Vertex.R;
                else if (e.getX() + Vertex.R > getWidth())
                    x = getWidth() - Vertex.R;
                else x = e.getX();
                if (e.getY() - Vertex.R < 0)
                    y = Vertex.R;
                else if (e.getY() + Vertex.R > getHeight())
                    y = getHeight() - Vertex.R;
                else y = e.getY();
                if (g.getVertexes().size() == 0) {
                    g.addVertex(new Vertex(x, y, (char) (g.getVertexes().size() + 'A'), Color.BLACK));
                } else
                    g.addVertex(new Vertex(x, y, (char) (g.getVertexes().get(g.getVertexes().size() - 1).getLetter() + 1), Color.BLACK));
                g.getVertexes().get(0);
            } else if (SwingUtilities.isRightMouseButton(e)) {
                if (line == null) {
                    for (Vertex v : g.getVertexes())
                        if (v.pointBeside(e.getX(), e.getY())) {
                            line = new Line(v.getX(), v.getY(), e.getX(), e.getY());
                            break;
                        }
                } else if (selectedVertexTo != null) {
                    addableEdge = new Edge(selectedVertexFrom, selectedVertexTo, isWheighted, isOriented);
                    if (isWheighted) {
                        num = new JFormattedTextField();
                        removeMouseListener(adapter);
                        removeMouseMotionListener(adapter);
                        num.setDocument(new NumberDocument());
                        num.addKeyListener(new KeyAdapter() {

                            @Override
                            public void keyReleased(KeyEvent e) {
                                if (!isWheighted) {
                                    remove(num);
                                    addMouseListener(adapter);
                                    addMouseMotionListener(adapter);
                                    repaint();
                                }
                                if (e.getKeyCode() == KeyEvent.VK_ENTER &&
                                        num.getText().length() > 0) {
                                    String buff = num.getText();
                                    int tmp = 0;
                                    try {
                                        tmp = Integer.parseInt(buff);
//                                            setCost(Integer.parseInt(buff));
                                    } catch (NumberFormatException exeption) {
                                        tmp = Integer.MAX_VALUE - 1;
                                    }
                                    if (tmp == 0) {
                                        JOptionPane.showMessageDialog(num,
                                                "Ввод нуля в качестве веса вершины запрещен", "Ошибка",
                                                JOptionPane.ERROR_MESSAGE);
                                        remove(num);
                                        addMouseListener(adapter);
                                        addMouseMotionListener(adapter);
                                        g.getEdges().remove(addableEdge);
                                        addableEdge=null;

                                    }
                                    else {
                                        setCost(tmp);
                                        remove(num);
                                        addMouseListener(adapter);
                                        addMouseMotionListener(adapter);
                                        addableEdge.setCost(cost);
                                    }
                                    repaint();
                                }
                            }
                        });
                        add(num);
                        num.setBounds((int) addableEdge.getCostPoint().getX(), (int) addableEdge.getCostPoint().getY(), 50, 20);
                        num.setVisible(true);
                        num.requestFocus();
                        repaint();
                    }


                    if (!g.getEdges().contains(addableEdge))
                        g.addEdge(addableEdge);
                    line = null;
                } else {
                    line = null;
                }
            }
            repaint();
        }

        int cost = 0;

        private void setCost(int cost) {
            this.cost = cost;
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            super.mouseReleased(e);
            repaint();
            prevV = null;
            prevY = 0;
            prevX = 0;
        }

        Vertex prevV = null;
        int prevX = 0;
        int prevY = 0;

        @Override
        public void mouseDragged(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                if (prevV == null) {
                    ArrayList<Vertex> vrtx = g.getVertexes();
                    for (Vertex v : vrtx) {
                        if (v.pointBeside(e.getX(), e.getY())) {
                            prevV = v;
                            break;
                        }
                    }
                } else {
                    int x = prevV.getX() + prevX - prevV.getX();
                    int y = prevV.getY() + prevY - prevV.getY();
                    if (x < 0)
                        prevV.setX(Vertex.R);
                    else if (x > getWidth() - Vertex.R)
                        prevV.setX(getWidth() - Vertex.R);
                    else prevV.setX(x);
                    if (y < 0)
                        prevV.setY(Vertex.R);
                    else if (y > getHeight() - Vertex.R)
                        prevV.setY(getHeight() - Vertex.R);
                    else prevV.setY(y);
                    ArrayList<Edge> edges = g.getEdges();
                    for (Edge ed : edges) {
                        ed.refresh();
                        repaint();
                    }
                }
                prevX = e.getX();
                prevY = e.getY();
            }
            repaint();
        }

        Vertex selectedVertexFrom = null;
        Vertex selectedVertexTo = null;

        @Override
        public void mouseMoved(MouseEvent e) {
            ArrayList<Vertex> vert = g.getVertexes();
            for (Vertex v : vert) {
                if (v.pointBeside(e.getX(), e.getY()) && selectedVertexFrom == null)
                    selectedVertexFrom = v;
                else if (selectedVertexFrom != null && v == selectedVertexFrom && line == null)
                    selectedVertexFrom = null;
            }
            if (line != null) {
                line.setEndX(e.getX());
                line.setEndY(e.getY());
                for (Vertex v : g.getVertexes())
                    if (v.pointBeside(e.getX(), e.getY()) && !v.equals(selectedVertexFrom)) {
                        selectedVertexTo = v;
                        break;
                    } else if (v == selectedVertexTo)
                        selectedVertexTo = null;
            }
            repaint();
        }
    }

    public void removeAdapter() {
        removeMouseListener(adapter);
        removeMouseMotionListener(adapter);
    }

    public void reestablishAdapter() {
        addMouseListener(adapter);
        addMouseMotionListener(adapter);
    }
}
